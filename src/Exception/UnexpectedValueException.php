<?php

/**
 * Tiat Framework
 *
 * @package        Tiat\Mail
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Mail\Exception;

/**
 *
 */
class UnexpectedValueException extends \UnexpectedValueException implements ExceptionInterface {

}
